package org.usfirst.frc.team1369.robot;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import org.usfirst.frc.team1369.robot.commands.CommandAutonomousDrive;
import org.usfirst.frc.team1369.robot.commands.CommandDrive;
import org.usfirst.frc.team1369.robot.commands.CommandLiftDown;
import org.usfirst.frc.team1369.robot.commands.CommandLiftUp;
import org.usfirst.frc.team1369.robot.commands.ExampleCommand;
import org.usfirst.frc.team1369.robot.commands.Teleop;
import org.usfirst.frc.team1369.robot.commands.ToggleGrabber;
import org.usfirst.frc.team1369.robot.subsystems.Drivetrain;
import org.usfirst.frc.team1369.robot.subsystems.ExampleSubsystem;
import org.usfirst.frc.team1369.robot.subsystems.Grabber;
import org.usfirst.frc.team1369.robot.subsystems.Lift;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {

	public static final ExampleSubsystem exampleSubsystem = new ExampleSubsystem();

	public static Drivetrain drivetrain = new Drivetrain();
	public static Lift lift = new Lift();
	public static Grabber grabber = new Grabber();
	boolean lastLiftSpeed = false;
	boolean lastGrabber = false;

	public static OI oi = new OI();

	public static Command getAutonomousCommand() {
		return autonomousCommand;
	}

	public Teleop teleop = new Teleop();
	public static Command autonomousCommand;

	private SendableChooser autoChooser;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	public void robotInit() {
		autoChooser = new SendableChooser();
		/*
		 * oi = new OI(); drivetrain = new Drivetrain(); lift = new Lift();
		 * grabber = new Grabber();
		 */
		// instantiate the command used for the autonomous period
		autonomousCommand = new ExampleCommand();

		autoChooser.addDefault("Do Nothing", new ExampleCommand());
		autoChooser.addObject("Drive Autonomously",
				new CommandAutonomousDrive());
		// autoChooser.addObject("Title", CommandClass());

		SmartDashboard.putData("Autonomous Chooser", autoChooser);
	}

	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	public void autonomousInit() {
		// schedule the autonomous command (example)
		autonomousCommand = (Command) autoChooser.getSelected();
		if (autonomousCommand != null)
			autonomousCommand.start();
	}

	/**
	 * This function is called periodically during autonomous
	 */
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
	}

	public void teleopInit() {
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
		if (autonomousCommand != null)
			autonomousCommand.cancel();
		teleop.start();

	}

	/**
	 * This function is called when the disabled button is hit. You can use it
	 * to reset subsystems before shutting down.
	 */
	public void disabledInit() {

	}

	/**
	 * This function is called periodically during operator control
	 */

	// private boolean isPushingTrigger = false;

	public void teleopPeriodic() {
		Scheduler.getInstance().run();
		Robot.drivetrain.tankDrive();
		//Uses slider on joystickFunction(3) to change speed of the lift
		if(Robot.oi.driveStickFunction.getThrottle() < .6){ //Makes sure the liftspeed is not where it would be zero
			RobotMap.LIFT_SPEED = (int)((((-Robot.oi.driveStickFunction.getThrottle()) + 1.0) / 2.0) * 100.0);//Changes the speed into a value between 0% and  100% speed
		}else{//If the liftspeed would be <= 20 then set it to 20
			RobotMap.LIFT_SPEED = 20;
		}
		
		
		// grabber open and close
		if (Robot.oi.driveStickFunction.getRawButton(1)) {
			if (!lastGrabber) {
				lastGrabber = true;

				if (Robot.grabber.s1.get()
						.equals(DoubleSolenoid.Value.kReverse)) {
					Robot.grabber.s1.set(DoubleSolenoid.Value.kForward);
					Robot.grabber.s2.set(DoubleSolenoid.Value.kForward);
				} else {
					Robot.grabber.s1.set(DoubleSolenoid.Value.kReverse);
					Robot.grabber.s2.set(DoubleSolenoid.Value.kReverse);
				}

			}
		} else {
			lastGrabber = false;
		}

		if (Robot.oi.driveStickFunction.getRawButton(RobotMap.LIFT_UP)) {
			Robot.lift.up();
		} else if (Robot.oi.driveStickFunction.getRawButton(RobotMap.LIFT_DOWN)) {
			Robot.lift.down();
		} else {
			Robot.lift.stop();
		}

		// toggleGrabber.whenPressed(new ToggleGrabber());

	}

	/**
	 * This function is called periodically during test mode
	 */
	public void testPeriodic() {
		LiveWindow.run();
	}
}
