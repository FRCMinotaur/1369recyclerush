package org.usfirst.frc.team1369.robot.commands;

import org.usfirst.frc.team1369.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

public class CommandLiftUp extends Command {

	public CommandLiftUp()
	{
		this.requires(Robot.lift);
	}
	
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void execute() {
		Robot.lift.up();

	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void end() {
		Robot.lift.stop();

	}

	@Override
	protected void interrupted() {
		Robot.lift.stop();

	}

}
