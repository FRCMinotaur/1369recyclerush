package org.usfirst.frc.team1369.robot.commands;

import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.timing.AutoCountdown;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;

public class CommandAutonomousDrive extends Command {
	private int speed = 20;

	public CommandAutonomousDrive() {
		this.requires(Robot.drivetrain);
	}

	@Override
	protected void initialize() {

		Robot.drivetrain.setTrain(speed, speed);
		AutoCountdown.main(4);

	}

	@Override
	protected void execute() {
		Robot.drivetrain.setTrain(speed, speed);
		System.out.println("kektus2");

	}

	@Override
	protected boolean isFinished() {
		return false;
	}

	@Override
	protected void end() {
		Robot.drivetrain.driveStop();
		System.out.println("kektus3");

	}

	@Override
	protected void interrupted() {
		Robot.drivetrain.setTrain(0, 0);

	}

}
