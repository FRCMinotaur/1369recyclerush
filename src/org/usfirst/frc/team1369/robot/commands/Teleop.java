
package org.usfirst.frc.team1369.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;

import org.usfirst.frc.team1369.robot.Robot;

/**
 *
 */
public class Teleop extends CommandGroup {

	public Teleop()
	
	{
		this.addParallel(new CommandDrive());

		//if the code is david (broken), remove this part
		this.addParallel(new CommandLiftUp());
		this.addParallel(new CommandLiftDown());
		this.addParallel(new CommandLiftStop());
		this.addParallel(new ToggleGrabber());
	}
	
}
