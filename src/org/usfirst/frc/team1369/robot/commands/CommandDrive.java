package org.usfirst.frc.team1369.robot.commands;

import org.usfirst.frc.team1369.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

public class CommandDrive extends Command
{

	public CommandDrive()
	{
		this.requires(Robot.drivetrain);
	}
	
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void execute() 
	{
		Robot.drivetrain.tankDrive();
	}

	@Override
	protected boolean isFinished() 
	{
		// this thing will never stop
		return false;
	}

	@Override
	protected void end() {
		// TODO Auto-generated method stub
		Robot.drivetrain.driveStop();
	}

	@Override
	protected void interrupted() {
		Robot.drivetrain.driveStop();
	}

}
