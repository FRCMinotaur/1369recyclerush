package org.usfirst.frc.team1369.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

import org.usfirst.frc.team1369.robot.commands.CommandLiftDown;
import org.usfirst.frc.team1369.robot.commands.CommandLiftUp;
import org.usfirst.frc.team1369.robot.commands.ExampleCommand;
import org.usfirst.frc.team1369.robot.commands.ToggleGrabber;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI 
{
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
	
	public Joystick driveStickLeft;
	public Joystick driveStickRight;
	public Joystick driveStickFunction;
	
	public OI()
	{
		driveStickLeft = new Joystick(RobotMap.JOYSTICK_RIGHT);
		driveStickRight = new Joystick(RobotMap.JOYSTICK_LEFT);
		driveStickFunction = new Joystick(RobotMap.JOYSTICK_FUNCTION);
		
		Button toggleGrabber = new JoystickButton(driveStickLeft, RobotMap.TOGGLE_GRABBER);
		Button liftUp = new JoystickButton(driveStickLeft, RobotMap.LIFT_UP_MANUAL_BUTTON);
		Button liftDown = new JoystickButton(driveStickLeft, RobotMap.LIFT_DOWN_MANUAL_BUTTON);
		
		/*toggleGrabber.whenPressed(new ToggleGrabber());
		liftUp.whileHeld(new CommandLiftUp());
		liftDown.whileHeld(new CommandLiftDown());*/
	}
	
}

