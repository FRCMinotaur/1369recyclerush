package org.usfirst.frc.team1369.robot;
/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // For example to map the left and right motors, you could define the
    // following variables to use with your drivetrain subsystem.
    // public static int leftMotor = 1;
    // public static int rightMotor = 2;
    
    // If you are using multiple modules, make sure to define both the port
    // number and the module. For example you with a rangefinder:
    // public static int rangefinderPort = 1;
    // public static int rangefinderModule = 1;

	//drive speeds
	public final static float HIGH_DRIVE_SPEED = 0.6f;
	public final static float LOW_DRIVE_SPEED = 0.25f;
	
	//drivetrain motor ports (PWM)
	public final static int LEFT_WHEEL_1_PORT = 0;
	public final static int LEFT_WHEEL_2_PORT = 1;
	public final static int RIGHT_WHEEL_1_PORT = 2;
	public final static int RIGHT_WHEEL_2_PORT = 3;
	
	//lift motor ports (PWM)
	public final static int LEFT_LIFT_MOTOR_PORT = 4;
	public final static int RIGHT_LIFT_MOTOR_PORT = 5;

	//(DIO PorT)
	public static int LIFT_ENCODER_A_PORT = 2;
	public static int LIFT_ENCODER_B_PORT = 3;
	
	public static double LIFT_WHEEL_RADIUS = .1565;
	public static double LIFT_ENCODER_CPR = 384;
	
	//lift limit switch port (DIO)
	public final static int LIFT_BOTTOM_SWITCH_PORT = 4;
	
	//grabber solenoid ports (PCM)
	public final static int LEFT_SOLENOID_A_PORT = 0;
	public final static int LEFT_SOLENOID_B_PORT = 1;
	public final static int RIGHT_SOLENOID_A_PORT = 2;
	public final static int RIGHT_SOLENOID_B_PORT = 3;
	
	
	
	public static enum Direction
	{
		LEFT,
		RIGHT,
		FORWARD,
		BACKWARD;
	}
	
	//BUTTONS for MANUAL CONTROLS (joy1)
	public final static int TOGGLE_GRABBER = 1;
	
	
	
	//JOYSTICK_3 (Functional)
	public final static int GEAR_SHIFT_UP = 6;
	public final static int GEAR_SHIFT_DOWN = 4;
	
	public final static int LIFT_UP = 5;
	public final static int LIFT_DOWN = 3;
	
	public final static int LIFT_25 = 11;
	public final static int LIFT_50 = 9;
	public final static int LIFT_100 = 7;
	public static int LIFT_SPEED = 100;
	
	
	
	public final static int LIFT_UP_MANUAL_BUTTON = 5;
	public final static int LIFT_DOWN_MANUAL_BUTTON = 3;
	
	//lift distances
	public final static float LIFT_TOP_DIST = 2.5f;
	public final static float TOTE_SET_DIST = 24f;
	
	//JOYSTICKS
	public final static int JOYSTICK_LEFT = 0;
	public final static int JOYSTICK_RIGHT = 1;
	public final static int JOYSTICK_FUNCTION = 2;
	
}
