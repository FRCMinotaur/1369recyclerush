package org.usfirst.frc.team1369.robot.subsystems;

import org.usfirst.frc.team1369.robot.RobotMap;

import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Lift extends Subsystem
{

	private Talon left;
	private Talon right;
	private Encoder encoder;
	
	public Lift()
	{
		left = new Talon(RobotMap.LEFT_LIFT_MOTOR_PORT);
		right = new Talon(RobotMap.RIGHT_LIFT_MOTOR_PORT);
		
		// Straight Outta Service BTW
		encoder = new Encoder(RobotMap.LIFT_ENCODER_A_PORT, RobotMap.LIFT_ENCODER_B_PORT, false, EncodingType.k4X);
		encoder.setDistancePerPulse((1 / RobotMap.LIFT_ENCODER_CPR * 2 * Math.PI * RobotMap.LIFT_WHEEL_RADIUS) / 5);
		encoder.setReverseDirection(true);
	}
	
	@Override
	protected void initDefaultCommand() 
	{
		// TODO Auto-generated method stub
		
	}
	
	public void up()
	{
		left.set(-RobotMap.LIFT_SPEED/100.0);
		right.set(RobotMap.LIFT_SPEED/100.0);
	}
	
	public void down()
	{
		left.set(RobotMap.LIFT_SPEED/100.0);
		right.set(-RobotMap.LIFT_SPEED/100.0);
	}
	
	public void stop()
	{
		left.set(0);
		right.set(0);
	}

}
