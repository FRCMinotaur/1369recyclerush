package org.usfirst.frc.team1369.robot.subsystems;

import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.RobotMap;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Drivetrain extends Subsystem 
{

	private Talon l1;
	private Talon l2;
	private Talon r1;
	private Talon r2;
	private RobotDrive drive;
	
	public Drivetrain()
	{
		l1 = new Talon(RobotMap.LEFT_WHEEL_1_PORT);
		l2 = new Talon(RobotMap.LEFT_WHEEL_2_PORT);
		r1 = new Talon(RobotMap.RIGHT_WHEEL_1_PORT);
		r2 = new Talon(RobotMap.RIGHT_WHEEL_2_PORT);
		drive = new RobotDrive(l1, l2, r1, r2);
		
		drive.setSafetyEnabled(true);
		drive.setExpiration(0.1);
		drive.setSensitivity(0.5);
		drive.setMaxOutput(1.0);
		
		drive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
		drive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
	}
	
	@Override
	protected void initDefaultCommand() 
	{
		
	}
	
	public void tankDrive()
	{
		//drive.tankDrive(Robot.oi.driveStickLeft.getY(), Robot.oi.driveStickRight.getY());
		l1.set(Robot.oi.driveStickRight.getY());
		l2.set(Robot.oi.driveStickRight.getY());
		r1.set(-Robot.oi.driveStickLeft.getY());
		r2.set(-Robot.oi.driveStickLeft.getY());
	}
	
	public void driveStop()
	{
		drive.drive(0, 0);
	}
	
	public void setTrain(int l, int r)
	{
		l1.set(l / 100.0);
		l2.set(l / 100.0);
		r1.set(-r / 100.0);
		r2.set(-r / 100.0);
	}

}
