
package org.usfirst.frc.team1369.robot.subsystems;

import org.usfirst.frc.team1369.robot.RobotMap;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class Grabber extends Subsystem {
    
    public DoubleSolenoid s1 = new DoubleSolenoid(RobotMap.LEFT_SOLENOID_A_PORT, RobotMap.LEFT_SOLENOID_B_PORT);
    public DoubleSolenoid s2 = new DoubleSolenoid(RobotMap.RIGHT_SOLENOID_A_PORT, RobotMap.RIGHT_SOLENOID_B_PORT);

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    
    public void release()
    {
    	s1.set(DoubleSolenoid.Value.kForward);
    	s2.set(DoubleSolenoid.Value.kForward);
    }
    
    public void grab()
    {
    	s1.set(DoubleSolenoid.Value.kReverse);
    	s2.set(DoubleSolenoid.Value.kReverse);
    }
    
}

